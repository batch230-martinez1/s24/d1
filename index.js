// console.log("Hello world");

// [SECTION] Exponent Operator
//ES6 update
const firstNum = 8**2;
console.log(firstNum);


const secondNum = Math.pow(8,2);
console.log(secondNum);


// [SECTION] Template Literals
/* 
    - Allows to write strings without using the concatenation oerator (+)
    - Greatly helps with code readibality

*/
// without
let name = "John";
let message = "Hello "+ name + "! Welcome to programming!";
console.log("Message without template literals: \n" + message);

//  String Using template literals
//  Uses backticks (``)
message = `Hello ${name}! Welcome to Programming!`;//ES6 update
console.log(`Message without template Literals: \n${message}`);

const anotherMessage = `${name} attended a math competition.\nHe won it by solving the problem 8**2 with the solution of ${firstNum}`
console.log(anotherMessage);

/* 
    -Template literals allows us to write strings with embedded javascript
    = expressions are any valid unit of code that resolves to a value   
    - "${}" are used to include Javasript expressions in strings using template literals
*/


const interestRate = .1;
const principal = 100;


console.log(`The interst of your savings account is ${principal * interestRate}`);

// [SECTION] Array Destructureing
/* 
    -Allows us to unpack elements in arrays distinct variables
    -Allows us to name array elements with, instead of  using index numbers
    - Helps ith code readability
    Syntax
        let/const [variableName, variableName, variableName]=  array;
*/

const fullName = ['Juan', 'Dela', "Cruz"];

// Pre-Array Destruction
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to see you`);

//  Array Destructuring //ES6 update

const  [firstName, middleName, lastName] = fullName
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);

//  [SECTION] Object Destructuring
/* 
    - Allow us to unpackproperties/keys of objects into distinct variables
    - Shortens the syntax for accessing properties from objects
    - Syntax
        let/const {propertyVariableName, propertyVariableName, propertyVariableName} = object;
    - Note-Rule:
        the variableName to be assigned in destructuring should be the same as the propertyName

*/

const person = {
    givenName: "jane",
    maidenName: "Dela",
    familyName: "Cruz"
}
// DESTRUCTURING // ES6
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object Destructuring // ES6 Update
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`hello ${givenName} ${maidenName} ${familyName}! It's good to see you again`)

// [SECTION] aRROW FUNCTIONS
/* 
    -Compact alternatives to traditional functions
    -Useful for code snippets where creating function will not be reused in any other portion of the code
    -Adhere to "DRY" ()

*/

const hello = () =>{
    console.log("Hello world");
}

hello();

const printFullName = (firstName, middleName, lastName) => {
    console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("John", "D", "Smith");
console.log("-------------------------");


// Arrow function in forEach  // ES6 Update
// Pre arrow Function

const
students.foreach(function(student){
    console.log(`${student} is a student.`);
})

//  Arrow function is ForEach // Es6 Update
//  The


// [SECTION]   Implicit Return Statement
/* 
    -There are instances when you can omit the 'return' statement
    - This work because  the 'return' statement Javascript implicity adds it for the result of the function
*/

//  Pre-arrow Function
/* const add = (numA, numB) => {
    return numA + numB;
}
let total = add(1,2);
console.log(total); */

const add = (numA, numB) => numA + numB;

let total = add(1,2);
console.log(total);

//  [SECTION] Default Function Argument


const greet = (name= 'User') => {
    return `Good morning, ${name}`

}
console.log(greet("John"));
console.log(greet());


//  [SECTION Clas-based Object Blueprints]
/* 
    -Allows creation/instantiation of objects using classes as blueprints
    Syntax:
        class className{
            constructor(objectPropertA, objectPropertyB){
                this.objectPropertyA = objectPropertyB
                this.objectPropertyB =objectPropertyA
            }
        }
*/

class Car {
    constructor(brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

//  Instantiating an object
const myCAr = new Car();
console.log(myCAr);

myCAr.brand = "Ford";
myCAr.name = "Range Raptor";
myCAr.year = "2021";
console.log(myCAr);


const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);

































